<?php

/**
 * Implements hook_variable_info().
 */
function duplicate_image_finder_variable_info($options)
{
    $variable = array();
    $controls = array(
        'dif_dir_path' => array(
            'type' => 'string',
            'text' => 'Directory path',
            'description' => 'Ex: sites/default/files/img'
        ),
        'dif_include_subdir' => array(
            'type' => 'boolean',
            'text' => 'Include subdirectories'
        )
    );
    foreach ($controls as $name => $arr) {
        $val = variable_get($name, '');
        $variable[$name] = array(
            'title' => t($arr['text']),
            'type' => $arr['type'],
            'description' => isset($arr['description']) ? $arr['description'] : '',
            'default' => is_array($val) && !empty($val) ? $val['value'] : $val,
            'group' => 'dif'
        );
    }

    return $variable;
}